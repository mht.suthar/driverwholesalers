import 'package:flutter/services.dart';

class MiscService {
  String drawableBasePath = "res/drawable/";
  String font = "Roboto";

  getFullScreen() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  revertFullScreen() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }
}
