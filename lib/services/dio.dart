
import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skeleton_flutter/constants/api/api.dart';

import 'storage.dart';

class DioService {
  late dio.Dio _dio;

  DioService() {
    _dio = dio.Dio(dio.BaseOptions(
        baseUrl: BASE_URL, connectTimeout: 120000, receiveTimeout: 120000));

    /**
     * set auth token if it exists in local
     */
    setAuthToken(Get.find<StorageService>().getAuthToken());

    /* _dio.interceptors.add(dio.InterceptorsWrapper(onRequest: (options) {
      try {
        debugPrint("${options.method} : ${options.uri}");
        if (options.data != null) debugPrint("data : ${options.data}");
        if (options.queryParameters.isNotEmpty)
          debugPrint("queries : ${options.queryParameters}");
        debugPrint("headers : ${options.headers}");
      } catch (e) {}
    }, onError: (error) {
      try {
        debugPrint("${error.error} : ${error.message}");
      } catch (e) {}
    }, onResponse: (response) {
      try {
        debugPrint("${response.statusCode} : ${response.data}");
      } catch (e) {}
    }));*/

    /**
     * set headers
     */
    _dio.options.headers.addAll({
      "Accept": "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    });

    _dio.interceptors.clear();
    _dio.interceptors
        .add(dio.InterceptorsWrapper(onRequest: (options, handler) {
      try {
        print("${options.method} : ${options.uri}");
        if (options.data != null) debugPrint("data : ${options.data}");
        if (options.data.fields != null)
          debugPrint("FormData : ${options.data.fields}");
        if (options.queryParameters.isNotEmpty)
          print("queries : ${options.queryParameters}");
        print("headers : ${options.headers}");
      } catch (e) {}
      return handler.next(options);
    }, onError: (error, handler) {
      try {
        print("${error.error} : ${error.message}");
      } catch (e) {}
      return handler.next(error);
    }, onResponse: (response, handler) {
      try {
        print("${response.statusCode} : ${response.data}");
      } catch (e) {}
      return handler.next(response);
    }));
  }

  Future<dio.Response<T>> get<T>(
          {required String url,
          Map<String, dynamic>? queryParams,
          bool isCompleteUrl = false}) =>
      isCompleteUrl
          ? _dio.getUri(Uri(path: url, queryParameters: queryParams))
          : _dio.get(url, queryParameters: queryParams);

  /// pass FormData.fromMap() as data in case of form data type
  Future<dio.Response<T>> post<T>(
          {required String url,
          data,
          Map<String, dynamic>? queryParams,
          bool isCompleteUrl = false}) =>
      isCompleteUrl
          ? _dio.postUri(Uri(path: url, queryParameters: queryParams),
              data: data,
              options: data is FormData
                  ? dio.Options(
                      contentType: dio.Headers.formUrlEncodedContentType)
                  : null)
          : _dio.post(url,
              queryParameters: queryParams,
              data: data,
              options: data is FormData
                  ? dio.Options(
                      contentType: dio.Headers.formUrlEncodedContentType)
                  : null);

  Future<dio.Response<T>> delete<T>(
          {required String url,
          data,
          Map<String, dynamic>? queryParams,
          bool isCompleteUrl = false}) =>
      isCompleteUrl
          ? _dio.deleteUri(Uri(path: url, queryParameters: queryParams),
              data: data)
          : _dio.delete(url, queryParameters: queryParams, data: data);

  Future<dio.Response<T>> patch<T>(
          {required String url,
          data,
          Map<String, dynamic>? queryParams,
          bool isCompleteUrl = false}) =>
      isCompleteUrl
          ? _dio.patchUri(Uri(path: url, queryParameters: queryParams),
              data: data)
          : _dio.patch(url, queryParameters: queryParams, data: data);

  setAuthToken(String? token) {
    if (token?.isNotEmpty ?? false)
      _dio.options.headers.addAll({"Authorization": "Bearer $token"});
  }
}
