import 'package:get/get.dart';
import 'package:skeleton_flutter/services/date_time.dart';
import 'package:skeleton_flutter/services/dio.dart';
import 'package:skeleton_flutter/services/storage.dart';
import 'package:skeleton_flutter/services/toast.dart';

class Locator {
  static void loadDependencies() {
    loadServices();
  }

  static void loadServices() {
    Get.lazyPut(() => StorageService());
    Get.lazyPut(() => DioService());
    Get.lazyPut(() => ToastService());
    Get.lazyPut(() => DateTimeService());
  }
}
