/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// ignore_for_file: directives_ordering

import 'package:flutter/painting.dart';
import 'package:flutter/material.dart';

class ColorName {
  ColorName._();

  static const MaterialColor accentColor = MaterialColor(
    0xFFFE8659,
    <int, Color>{
      50: Color(0xFFFFF0EB),
      100: Color(0xFFFFDBCD),
      200: Color(0xFFFFC3AC),
      300: Color(0xFFFEAA8B),
      400: Color(0xFFFE9872),
      500: Color(0xFFFE8659),
      600: Color(0xFFFE7E51),
      700: Color(0xFFFE7348),
      800: Color(0xFFFE693E),
      900: Color(0xFFFD562E),
    },
  );
  static const MaterialColor blueColor = MaterialColor(
    0xFF74DCD3,
    <int, Color>{
      50: Color(0xFFEEFBFA),
      100: Color(0xFFD5F5F2),
      200: Color(0xFFBAEEE9),
      300: Color(0xFF9EE7E0),
      400: Color(0xFF89E1DA),
      500: Color(0xFF74DCD3),
      600: Color(0xFF6CD8CE),
      700: Color(0xFF61D3C8),
      800: Color(0xFF57CEC2),
      900: Color(0xFF44C5B7),
    },
  );
  static const Color cinnamon = Color(0xFFF5CB84);
  static const MaterialColor greyBg = MaterialColor(
    0xFFF6F9F8,
    <int, Color>{
      50: Color(0xFFFEFEFE),
      100: Color(0xFFFCFDFD),
      200: Color(0xFFFBFCFC),
      300: Color(0xFFF9FBFA),
      400: Color(0xFFF7FAF9),
      500: Color(0xFFF6F9F8),
      600: Color(0xFFF5F8F7),
      700: Color(0xFFF3F7F6),
      800: Color(0xFFF2F6F5),
      900: Color(0xFFEFF5F3),
    },
  );
  static const MaterialColor greyColor = MaterialColor(
    0xFFF4F4F4,
    <int, Color>{
      50: Color(0xFFFEFEFE),
      100: Color(0xFFFCFCFC),
      200: Color(0xFFFAFAFA),
      300: Color(0xFFF7F7F7),
      400: Color(0xFFF6F6F6),
      500: Color(0xFFF4F4F4),
      600: Color(0xFFF3F3F3),
      700: Color(0xFFF1F1F1),
      800: Color(0xFFEFEFEF),
      900: Color(0xFFECECEC),
    },
  );
  static const MaterialColor greyTextfieldColor = MaterialColor(
    0xFF777777,
    <int, Color>{
      50: Color(0xFFEFEFEF),
      100: Color(0xFFD6D6D6),
      200: Color(0xFFBBBBBB),
      300: Color(0xFFA0A0A0),
      400: Color(0xFF8B8B8B),
      500: Color(0xFF777777),
      600: Color(0xFF6F6F6F),
      700: Color(0xFF646464),
      800: Color(0xFF5A5A5A),
      900: Color(0xFF474747),
    },
  );
  static const MaterialColor pinkColor = MaterialColor(
    0xFFFFE2D8,
    <int, Color>{
      50: Color(0xFFFFFCFA),
      100: Color(0xFFFFF6F3),
      200: Color(0xFFFFF1EC),
      300: Color(0xFFFFEBE4),
      400: Color(0xFFFFE6DE),
      500: Color(0xFFFFE2D8),
      600: Color(0xFFFFDFD4),
      700: Color(0xFFFFDACE),
      800: Color(0xFFFFD6C8),
      900: Color(0xFFFFCFBF),
    },
  );
  static const MaterialColor splashBgColor = MaterialColor(
    0xFF74DCD3,
    <int, Color>{
      50: Color(0xFFEEFBFA),
      100: Color(0xFFD5F5F2),
      200: Color(0xFFBAEEE9),
      300: Color(0xFF9EE7E0),
      400: Color(0xFF89E1DA),
      500: Color(0xFF74DCD3),
      600: Color(0xFF6CD8CE),
      700: Color(0xFF61D3C8),
      800: Color(0xFF57CEC2),
      900: Color(0xFF44C5B7),
    },
  );
}
