// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of '../pages/base/api_call_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ApiCallStateTearOff {
  const _$ApiCallStateTearOff();

// ignore: unused_element
  Idle<T> idle<T>() {
    return Idle<T>();
  }

// ignore: unused_element
  Success<T> success<T>(T response, int reqCode) {
    return Success<T>(
      response,
      reqCode,
    );
  }

// ignore: unused_element
  Loading<T> loading<T>() {
    return Loading<T>();
  }

// ignore: unused_element
  Error<T> error<T>(String message) {
    return Error<T>(
      message,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ApiCallState = _$ApiCallStateTearOff();

/// @nodoc
mixin _$ApiCallState<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    required TResult idle(),
    required TResult success(T response, int reqCode),
    required TResult loading(),
    required TResult error(String message),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult idle()?,
    TResult success(T response, int reqCode)?,
    TResult loading()?,
    TResult error(String message)?,
    required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    required TResult idle(Idle<T> value),
    required TResult success(Success<T> value),
    required TResult loading(Loading<T> value),
    required TResult error(Error<T> value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult idle(Idle<T> value)?,
    TResult success(Success<T> value)?,
    TResult loading(Loading<T> value)?,
    TResult error(Error<T> value)?,
    required TResult orElse(),
  });
}

/// @nodoc
abstract class $ApiCallStateCopyWith<T, $Res> {
  factory $ApiCallStateCopyWith(
          ApiCallState<T> value, $Res Function(ApiCallState<T>) then) =
      _$ApiCallStateCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$ApiCallStateCopyWithImpl<T, $Res>
    implements $ApiCallStateCopyWith<T, $Res> {
  _$ApiCallStateCopyWithImpl(this._value, this._then);

  final ApiCallState<T> _value;
  // ignore: unused_field
  final $Res Function(ApiCallState<T>) _then;
}

/// @nodoc
abstract class $IdleCopyWith<T, $Res> {
  factory $IdleCopyWith(Idle<T> value, $Res Function(Idle<T>) then) =
      _$IdleCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$IdleCopyWithImpl<T, $Res> extends _$ApiCallStateCopyWithImpl<T, $Res>
    implements $IdleCopyWith<T, $Res> {
  _$IdleCopyWithImpl(Idle<T> _value, $Res Function(Idle<T>) _then)
      : super(_value, (v) => _then(v as Idle<T>));

  @override
  Idle<T> get _value => super._value as Idle<T>;
}

/// @nodoc
class _$Idle<T> with DiagnosticableTreeMixin implements Idle<T> {
  const _$Idle();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiCallState<$T>.idle()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'ApiCallState<$T>.idle'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Idle<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    required TResult idle(),
    required TResult success(T response, int reqCode),
    required TResult loading(),
    required TResult error(String message),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return idle();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult idle()?,
    TResult success(T response, int reqCode)?,
    TResult loading()?,
    TResult error(String message)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    required TResult idle(Idle<T> value),
    required TResult success(Success<T> value),
    required TResult loading(Loading<T> value),
    required TResult error(Error<T> value),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return idle(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult idle(Idle<T> value)?,
    TResult success(Success<T> value)?,
    TResult loading(Loading<T> value)?,
    TResult error(Error<T> value)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (idle != null) {
      return idle(this);
    }
    return orElse();
  }
}

abstract class Idle<T> implements ApiCallState<T> {
  const factory Idle() = _$Idle<T>;
}

/// @nodoc
abstract class $SuccessCopyWith<T, $Res> {
  factory $SuccessCopyWith(Success<T> value, $Res Function(Success<T?>) then) =
      _$SuccessCopyWithImpl<T, $Res>;
  $Res call({T? response, int? reqCode});
}

/// @nodoc
class _$SuccessCopyWithImpl<T, $Res> extends _$ApiCallStateCopyWithImpl<T?, $Res>
    implements $SuccessCopyWith<T, $Res> {
  _$SuccessCopyWithImpl(Success<T> _value, $Res Function(Success<T?>) _then)
      : super(_value, (v) => _then(v as Success<T?>));

  @override
  Success<T?> get _value => super._value as Success<T?>;

  @override
  $Res call({
    Object? response = freezed,
    Object? reqCode = freezed,
  }) {
    return _then(Success<T?>(
      response == freezed ? _value.response : response as T?,
      reqCode == freezed ? _value.reqCode : (reqCode as int?)!,
    ));
  }
}

/// @nodoc
class _$Success<T> with DiagnosticableTreeMixin implements Success<T> {
  const _$Success(this.response, this.reqCode)
      : assert(response != null),
        assert(reqCode != null);

  @override
  final T response;
  @override
  final int reqCode;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiCallState<$T>.success(response: $response, reqCode: $reqCode)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiCallState<$T>.success'))
      ..add(DiagnosticsProperty('response', response))
      ..add(DiagnosticsProperty('reqCode', reqCode));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Success<T> &&
            (identical(other.response, response) ||
                const DeepCollectionEquality()
                    .equals(other.response, response)) &&
            (identical(other.reqCode, reqCode) ||
                const DeepCollectionEquality().equals(other.reqCode, reqCode)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(response) ^
      const DeepCollectionEquality().hash(reqCode);

  @override
  $SuccessCopyWith<T, Success<T>> get copyWith =>
      _$SuccessCopyWithImpl<T, Success<T>>(this, _$identity as Success<T> Function(Success<T?>));

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    required TResult idle(),
    required TResult success(T response, int reqCode),
    required TResult loading(),
    required TResult error(String message),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return success(response, reqCode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult idle()?,
    TResult success(T response, int reqCode)?,
    TResult loading()?,
    TResult error(String message)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(response, reqCode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    required TResult idle(Idle<T> value),
    required TResult success(Success<T> value),
    required TResult loading(Loading<T> value),
    required TResult error(Error<T> value),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult idle(Idle<T> value)?,
    TResult success(Success<T> value)?,
    TResult loading(Loading<T> value)?,
    TResult error(Error<T> value)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class Success<T> implements ApiCallState<T> {
  const factory Success(T response, int reqCode) = _$Success<T>;

  T get response;
  int get reqCode;
  $SuccessCopyWith<T, Success<T>> get copyWith;
}

/// @nodoc
abstract class $LoadingCopyWith<T, $Res> {
  factory $LoadingCopyWith(Loading<T> value, $Res Function(Loading<T>) then) =
      _$LoadingCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$LoadingCopyWithImpl<T, $Res> extends _$ApiCallStateCopyWithImpl<T, $Res>
    implements $LoadingCopyWith<T, $Res> {
  _$LoadingCopyWithImpl(Loading<T> _value, $Res Function(Loading<T>) _then)
      : super(_value, (v) => _then(v as Loading<T>));

  @override
  Loading<T> get _value => super._value as Loading<T>;
}

/// @nodoc
class _$Loading<T> with DiagnosticableTreeMixin implements Loading<T> {
  const _$Loading();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiCallState<$T>.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'ApiCallState<$T>.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Loading<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    required TResult idle(),
    required TResult success(T response, int reqCode),
    required TResult loading(),
    required TResult error(String message),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult idle()?,
    TResult success(T response, int reqCode)?,
    TResult loading()?,
    TResult error(String message)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    required TResult idle(Idle<T> value),
    required TResult success(Success<T> value),
    required TResult loading(Loading<T> value),
    required TResult error(Error<T> value),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult idle(Idle<T> value)?,
    TResult success(Success<T> value)?,
    TResult loading(Loading<T> value)?,
    TResult error(Error<T> value)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading<T> implements ApiCallState<T> {
  const factory Loading() = _$Loading<T>;
}

/// @nodoc
abstract class $ErrorCopyWith<T, $Res> {
  factory $ErrorCopyWith(Error<T> value, $Res Function(Error<T>) then) =
      _$ErrorCopyWithImpl<T, $Res>;
  $Res call({String? message});
}

/// @nodoc
class _$ErrorCopyWithImpl<T, $Res> extends _$ApiCallStateCopyWithImpl<T, $Res>
    implements $ErrorCopyWith<T, $Res> {
  _$ErrorCopyWithImpl(Error<T> _value, $Res Function(Error<T>) _then)
      : super(_value, (v) => _then(v as Error<T>));

  @override
  Error<T> get _value => super._value as Error<T>;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(Error<T>(
      message == freezed ? _value.message : (message as String?)!,
    ));
  }
}

/// @nodoc
class _$Error<T> with DiagnosticableTreeMixin implements Error<T> {
  const _$Error(this.message) : assert(message != null);

  @override
  final String message;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ApiCallState<$T>.error(message: $message)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ApiCallState<$T>.error'))
      ..add(DiagnosticsProperty('message', message));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Error<T> &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @override
  $ErrorCopyWith<T, Error<T>> get copyWith =>
      _$ErrorCopyWithImpl<T, Error<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    required TResult idle(),
    required TResult success(T response, int reqCode),
    required TResult loading(),
    required TResult error(String message),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult idle()?,
    TResult success(T response, int reqCode)?,
    TResult loading()?,
    TResult error(String message)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    required TResult idle(Idle<T> value),
    required TResult success(Success<T> value),
    required TResult loading(Loading<T> value),
    required TResult error(Error<T> value),
  }) {
    assert(idle != null);
    assert(success != null);
    assert(loading != null);
    assert(error != null);
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult idle(Idle<T> value)?,
    TResult success(Success<T> value)?,
    TResult loading(Loading<T> value)?,
    TResult error(Error<T> value)?,
    required TResult orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class Error<T> implements ApiCallState<T> {
  const factory Error(String message) = _$Error<T>;

  String get message;
  $ErrorCopyWith<T, Error<T>> get copyWith;
}
