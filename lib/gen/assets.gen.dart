/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// ignore_for_file: directives_ordering

import 'package:flutter/widgets.dart';

class $ResGen {
  const $ResGen();

  $ResDrawablesGen get drawables => const $ResDrawablesGen();
}

class $ResDrawablesGen {
  const $ResDrawablesGen();

  AssetGenImage get bgBlueLogin =>
      const AssetGenImage('res/drawables/bg_blue_login.png');
  AssetGenImage get bgLogin =>
      const AssetGenImage('res/drawables/bg_login.png');
  AssetGenImage get icCamera =>
      const AssetGenImage('res/drawables/ic_camera.png');
  AssetGenImage get icGoogle =>
      const AssetGenImage('res/drawables/ic_google.jpg');
  AssetGenImage get icPinGreen =>
      const AssetGenImage('res/drawables/ic_pin_green.png');
  AssetGenImage get icPinRed =>
      const AssetGenImage('res/drawables/ic_pin_red.png');
  AssetGenImage get icPinSource =>
      const AssetGenImage('res/drawables/ic_pin_source.png');
}

class Assets {
  Assets._();

  static const $ResGen res = $ResGen();
}

class AssetGenImage extends AssetImage {
  const AssetGenImage(String assetName) : super(assetName);

  Image image({
    Key? key,
    ImageFrameBuilder? frameBuilder,
    ImageLoadingBuilder? loadingBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? width,
    double? height,
    Color? color,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
  }) {
    return Image(
      key: key,
      image: this,
      frameBuilder: frameBuilder,
      loadingBuilder: loadingBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      filterQuality: filterQuality,
    );
  }

  String get path => assetName;
}
