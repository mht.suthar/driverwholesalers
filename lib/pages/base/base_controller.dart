import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skeleton_flutter/services/dio.dart';
import 'package:skeleton_flutter/services/storage.dart';
import 'package:skeleton_flutter/services/toast.dart';

abstract class BaseController extends GetxController {
  var isProgressShowing = false.obs;

  @protected
  var api = Get.find<DioService>();
  @protected
  final storage = Get.find<StorageService>();
  final _toast = Get.find<ToastService>();

  successToast(String message) {
    _toast.successToast(message);
  }

  errorToast(String message) {
    _toast.errorToast(message);
  }

  Future<dynamic> callApi(Future<dio.Response> apiCall, {onError(dynamic)?}) async {
    try {
      isProgressShowing.value = true;
      if (await _isInternetConnected()) {
        var response = (await apiCall).data;
        isProgressShowing.value = false;
        return response;
      }
    } catch (e) {
      if (onError == null)
        this.onError(e);
      else
        onError(e);
    }
    isProgressShowing.value = false;
    return null;
  }

  void onError(error) {
    if (error is dio.DioError)
      _toast.errorToast(error.error.toString());
    else if (error is HttpException)
      _toast.errorToast(error.message);
    else {
      //parse response error
    }
  }

  Future<bool> _isInternetConnected() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi)
      return true;
    else
      throw HttpException("Internet not connected");
  }
}
