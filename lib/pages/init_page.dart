
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'login/login.dart';
import 'login/login_controller.dart';

class InitPage extends StatelessWidget{

  final LoginController _controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(360, 690),
        orientation: Orientation.portrait);
    //ScreenUtil.init(context, width: 375, height: 667, allowFontScaling: false);  //set width and height of artboard

    return _controller.isLogIn() ? SegmentListScreen() : LoginScreen();
  }
}