import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/data/response/segment_data.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';
import 'package:skeleton_flutter/pages/login/login.dart';

class SegmentController  extends BaseController{

  var listOfSegment = <Segments>[].obs;

  getSegmentList() async{
    var segmentListData = await callApi(
        api.get(
            url: GET_SEGMENT + "/" + storage.getUserId()!,
            isCompleteUrl: true), onError: (e) {
      //state(ApiCallState.error("failed"));
    });
    if (segmentListData != null) {
      var apiResModel = ApiResModel.fromJson(segmentListData);
      var segmentData = SegmentData.fromJson(apiResModel.data);
      listOfSegment.clear();
      if(segmentData.segments != null) {
        listOfSegment.addAll(segmentData.segments!);
      }
    }
  }

  void logoutDialog() {
    Get.dialog(
      AlertDialog(
        title: Text("logout".tr),
        content: Text("are_you_sure_logout_".tr),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "no".tr,
            ),
          ),
          TextButton(
            onPressed: () {
              _logout();
            },
            child: Text(
              "logout".tr,
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }

  _logout() async {
    await storage.clear();
    Get.offAll(LoginScreen());
  }
}