import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:skeleton_flutter/gen/assets.gen.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';
import 'package:skeleton_flutter/pages/navigation/navigation.dart';
import 'package:skeleton_flutter/pages/segment/segment_controller.dart';

class SegmentListScreen extends StatefulWidget {
  const SegmentListScreen({Key? key}) : super(key: key);

  @override
  _SegmentListScreenState createState() => _SegmentListScreenState();
}

class _SegmentListScreenState extends State<SegmentListScreen> {
  final SegmentController _controller = Get.put(SegmentController());

  @override
  void initState() {
    super.initState();
    _controller.getSegmentList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(Assets.res.drawables.bgBlueLogin.assetName),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            height: 40.h,
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 16),
              child: InkWell(
                child: Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.white,
                ),
                onTap: () {
                  _controller.logoutDialog();
                },
              ),
            ),
          ),
          SizedBox(
            height: 50.h,
          ),
          Text(
            "segment".tr,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 55.sp,
                color: Colors.white,
                fontWeight: FontWeight.w700),
          ),
          SizedBox(
            height: 30.h,
          ),
          Expanded(
            child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(30.0),
              child: Obx(() => _controller.listOfSegment.length == 0
                  ? _progressBar()
                  : ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.only(top: 10),
                      itemCount: _controller.listOfSegment.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _rowSegment(index);
                      },
                    )),
            ),
          )
        ],
      ),
    ));
  }

  Widget _rowSegment(int index) {
    return ListTile(
      title: Text(
        '${_controller.listOfSegment[index].segmentTitle}',
        textScaleFactor: 1.5,
      ),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: () {
        Get.off(NavigationSource(),
            arguments: [_controller.listOfSegment[index].segmentId]);
      },
    );
  }

  _progressBar() {
    return Obx(() => Center(
          child: (_controller.isProgressShowing.value)
              ? SizedBox(
                  width: 35.r,
                  height: 35.r,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(ColorName.accentColor),
                    strokeWidth: 5.w,
                  ))
              : Container(),
        ));
  }
}
