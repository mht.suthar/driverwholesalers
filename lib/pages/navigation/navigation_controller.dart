import 'dart:async';
import 'dart:math';

import 'package:collection/collection.dart';
import 'package:dio/dio.dart' as dio;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fl_location/fl_location.dart' as geoLocation;
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:maps_toolkit/maps_toolkit.dart' as mapTool;
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';
import 'package:skeleton_flutter/pages/home/home_controller.dart';
import 'package:skeleton_flutter/pages/location/location_details.dart';
import 'package:skeleton_flutter/pages/login/login.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'package:wakelock/wakelock.dart';

class NavigationController extends BaseController {
  List<SegmentLocation> listOfSegmentLocation = [];
  List<SegmentLocation> listOfSkipSegmentLocation = [];
  late LocationData currentLocation;
  final List<LatLng> allPolylineCoordinates = [];
  final List<mapTool.LatLng> allPolylineCoordinatesMapTool = [];
  static const SOURCE = "source";
  StreamController<MapRefreshType> isMapRefresh =
      StreamController<MapRefreshType>();
  var maxSpeed = "50";

  // Map storing polylines created by connecting two points
  Map<PolylineId, Polyline> polylines = {};
  Map<String, Marker> markers = {};
  Map<String, Marker> currentMarkers = {};
  var isProgressMap = false.obs;
  var userTouchMap = false.obs;

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _geofenceService
          .addGeofenceStatusChangeListener(_onGeofenceStatusChanged);
    });
    _getMaxSpeed();
  }

  Future<void> addMarker(
      LatLng latLng, String id, String title, bool isCompleted) async {
    //If completed than show green marker otherwise red
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        isCompleted
            ? 'res/drawables/ic_pin_green.png'
            : 'res/drawables/ic_pin_red.png');
    markers[id] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(id),
        position: latLng,
        infoWindow: InfoWindow(
          title: '' + title,
        ),
        icon: bitmapIcon,
        onTap: () {
          var segmentLocation = _getSegmentLocation(id);
          if (segmentLocation != null)
            Get.to(LocationDetails(segmentLocation: segmentLocation))
                ?.then((value) => {_handleLocationDetailChanges(value)});
        });
  }

  Future<void> addCurrentMarker(LatLng latLng) async {
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(80, 80)),
        'res/drawables/ic_pin_source.png');
    var marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: MarkerId(SOURCE),
      position: latLng,
      infoWindow: InfoWindow(
        title: 'Your Location',
      ),
      icon: bitmapIcon,
    );
    //markers[SOURCE] = marker;
    currentMarkers[SOURCE] = marker;
  }

  _handleLocationDetailChanges(value) async {
    var segLocation = value as SegmentLocation;

    ///If User skip location than add to the new list
    if (segLocation.isSkiped == true) {
      if (!listOfSkipSegmentLocation.contains(segLocation))
        listOfSkipSegmentLocation.add(segLocation);
    }
    //Update List with Updated Value
    listOfSegmentLocation[listOfSegmentLocation.indexWhere(
            (element) => element.address_id == segLocation.address_id)] =
        segLocation;

    //Update Marker Also
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        segLocation.completed == "1"
            ? 'res/drawables/ic_pin_green.png'
            : 'res/drawables/ic_pin_red.png');

    markers[segLocation.address_id ?? ""] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(segLocation.address_id ?? ""),
        position: LatLng(double.parse(segLocation.lat ?? "0"),
            double.parse(segLocation.lng ?? "0")),
        infoWindow: InfoWindow(
          title: segLocation.address,
        ),
        icon: bitmapIcon,
        onTap: () {
          Get.to(LocationDetails(segmentLocation: segLocation))
              ?.then((value) => {_handleLocationDetailChanges(value)});
        });
    //refreshMapForPoly();
    ///Refresh map with recenter
    isMapRefresh.add(MapRefreshType.RECENTER);
    _setPolyLines();
  }

  refreshMapForPoly() {
    isMapRefresh.add(MapRefreshType.REFRESH);
  }

  /// Cancel All the data here
  void cancelAll() async {
    isMapRefresh.close();
    _geofenceService.clearGeofenceList();
    _geofenceService.stop();
    Wakelock.disable();
  }

  void refreshPolyline() {
    _setPolyLines();
  }

  void _setPolyLines() async {
    if (listOfSegmentLocation.isNotEmpty) {
      _sortLocations();

      ///If not completed or not skiped
      var segmentData = listOfSegmentLocation.firstWhereOrNull(
          (element) => element.completed == "0" && element.isSkiped == false);
      if (segmentData == null) {
        if (listOfSkipSegmentLocation.isNotEmpty) {
          _showSkipLocationDialog();
          return;
        }
        //Complete All
        _showCompleteJourneyDialog();
        return;
      }
      isProgressMap.value = true;
      //23.120572314017064, 72.56506443226404
      final polylinePoints = PolylinePoints();
      final startPoint = PointLatLng(
          currentLocation.latitude ?? 0, currentLocation.longitude ?? 0);
      final finishPoint = PointLatLng(double.parse(segmentData.lat ?? "0"),
          double.parse(segmentData.lng ?? "0"));
      final result = await polylinePoints.getRouteBetweenCoordinates(
          GoogleAPIKey, startPoint, finishPoint,
          optimizeWaypoints: true);
      if (result.points.isNotEmpty) {
        allPolylineCoordinates.clear();
        allPolylineCoordinatesMapTool.clear();
        result.points.forEach((PointLatLng point) {
          allPolylineCoordinates.add(
            LatLng(point.latitude, point.longitude),
          );
          allPolylineCoordinatesMapTool.add(
            mapTool.LatLng(point.latitude, point.longitude),
          );
        });
      }
      print("Poly Point ${allPolylineCoordinates.length}");
      isProgressMap.value = false;
      refreshMapForPoly();
    }
  }

  ///Sort the location which is near from currunt location
  _sortLocations() {
    /*listOfSegmentLocation.sort((a, b) {
      return calculateDistance(
          currentLocation.latitude!,
          currentLocation.longitude!,
          double.parse(a.lat ?? "0"),
          double.parse(a.lng ?? "0"))
          .compareTo(calculateDistance(
          currentLocation.latitude!,
          currentLocation.longitude!,
          double.parse(b.lat ?? "0"),
          double.parse(b.lng ?? "0")));
    });*/
  }

  final _geofenceService = GeofenceService.instance.setup(
      interval: 5000,
      accuracy: 100,
      loiteringDelayMs: 60000,
      statusChangeDelayMs: 10000,
      useActivityRecognition: true,
      allowMockLocations: false,
      printDevLog: false,
      geofenceRadiusSortType: GeofenceRadiusSortType.DESC);

  /// This function is to be called when the geofence status is changed.
  Future<void> _onGeofenceStatusChanged(
      Geofence geofence,
      GeofenceRadius geofenceRadius,
      GeofenceStatus geofenceStatus,
      geoLocation.Location location) async {
    print('geofence: ${geofence.toJson()}');
    print('geofenceRadius: ${geofenceRadius.toJson()}');
    print('geofenceStatus: ${geofenceStatus.toString()}');
    if (geofenceStatus == GeofenceStatus.ENTER) {
      _makeMarkerGreen(geofence);
    }
  }

  getGeofenceService() => _geofenceService;

  _addGeoFenceLocation(LatLng latLng, String addressID) async {
    var geofenceData = Geofence(
      id: addressID,
      latitude: latLng.latitude,
      longitude: latLng.longitude,
      radius: [
        //GeofenceRadius(id: 'radius_60m', length: 60),
        GeofenceRadius(id: 'radius_25m', length: 25),
      ],
    );
    _geofenceService.addGeofence(geofenceData);
  }

  _removeGeofenceLocation(Geofence entry) async {
    _geofenceService.removeGeofence(entry);
  }

  SegmentLocation? _getSegmentLocation(id) {
    return listOfSegmentLocation
        .singleWhere((element) => element.address_id == id);
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  void _showCompleteJourneyDialog() {
    if (isProgressMap.value) isProgressMap.value = false;
    Get.dialog(
      AlertDialog(
        title: Text("Completed"),
        content: Text("You have completed all the Locations"),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.off(SegmentListScreen());
            },
            child: Text(
              "Back",
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }

  void logoutDialog() {
    Get.dialog(
      AlertDialog(
        title: Text("logout".tr),
        content: Text("are_you_sure_logout_".tr),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "no".tr,
            ),
          ),
          TextButton(
            onPressed: () {
              _logout();
            },
            child: Text(
              "logout".tr,
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }

  _logout() async {
    await storage.clear();
    Get.offAll(LoginScreen());
  }

  void showNotificationDialog(RemoteMessage message) {
    Get.dialog(
      AlertDialog(
        title: Text(message.notification?.title ?? "notification".tr),
        content: Text("${message.notification?.body}"),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.back();
            },
            child: Text(
              "ok".tr,
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }

  _getMaxSpeed() async {
    var maxSpeedData = await callApi(api.get(url: GET_MAX_SPEED));
    if (maxSpeedData != null) {
      var apiResModel = ApiResModel.fromJson(maxSpeedData);
      if (apiResModel.status == true) {
        maxSpeed = (apiResModel.data as Map)["speed"].toString();
        print("Max Speed $maxSpeed");
      }
    }
  }

  _makeMarkerGreen(Geofence entry) async {
    final bitmapIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(75, 75)),
        'res/drawables/ic_pin_green.png');

    var segmentLocation = _getSegmentLocation(entry.id);
    segmentLocation?.setCompleted("1");
    markers[entry.id] = Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(entry.id),
        position: LatLng(entry.latitude, entry.longitude),
        infoWindow: InfoWindow(
          title: segmentLocation?.address ?? "",
        ),
        icon: bitmapIcon,
        onTap: () {
          if (segmentLocation != null)
            Get.to(LocationDetails(segmentLocation: segmentLocation));
        });

    ///Update in global list aswell
    if (segmentLocation != null)
      listOfSegmentLocation[listOfSegmentLocation.indexWhere(
          (element) => element.address_id == entry.id)] = segmentLocation;

    ///Make API Call and Completed
    _makeCompletedThisMarker(entry.id);
    _removeGeofenceLocation(entry);
    refreshMapForPoly();

    ///One Completed than traverse next and draw next path
    _setPolyLines();
  }

  void _makeCompletedThisMarker(addressId) async {
    var formData = dio.FormData.fromMap({
      "AddressId": addressId,
      "ImageFile": "",
      "isCompleted": "1",
    });
    var markerCompletedData = await callApi(
        api.post(url: MARK_COMPLETED, data: formData),
        onError: (e) {});
    if (markerCompletedData != null) {
      var apiResModel = ApiResModel.fromJson(markerCompletedData);
    }
  }

  getUserLatLngFromServer(dynamic arguments) async {
    var latLngData = await callApi(
        api.get(
            url: GET_SEGMENT_LOCATION + "/" + arguments[0],
            isCompleteUrl: true), onError: (e) {
      //state(ApiCallState.error("failed"));
    });
    if (latLngData != null) {
      isProgressMap.value = true;
      var apiResModel = ApiResModel.fromJson(latLngData);
      var sourceDestinationData =
          SourceDestinationData.fromJson(apiResModel.data);

      if (sourceDestinationData.listoflatlng != null) {
        listOfSegmentLocation = sourceDestinationData.listoflatlng!;
        _sortLocations();

        ///Add Marker and Add Geofence
        for (var source in listOfSegmentLocation) {
          //First Add Marker
          var latlng =
              LatLng(double.parse(source.lat!), double.parse(source.lng!));
          //Add Marker Here
          await addMarker(latlng, "" + source.address_id!, "" + source.address!,
              source.completed == "1");
          //Add Geofence Data Here
          await _addGeoFenceLocation(latlng, source.address_id ?? "");
        }
        _geofenceService.start();
        Wakelock.enable();
        isProgressMap.value = false;
        _setPolyLines();
      }
    }
  }

  void reportSpeedViolation(speed) async {
    var formData = dio.FormData.fromMap({
      "DriverId": storage.getUserId(),
      "Speed": speed,
      "Latitude": currentLocation.latitude,
      "Longitude": currentLocation.longitude,
    });
    var speedViolationData = await callApi(api.post(
        url: REPORT_SPEED_VIOLATION,
        data:
            formData)); //remove onError if error should be handled automatically (i.e. toast)
    if (speedViolationData != null) {
      var apiResModel = ApiResModel.fromJson(speedViolationData);
      if (apiResModel.status == true) {
        //Violation Send Succ
      }
    }
  }

  void _showSkipLocationDialog() {
    if (isProgressMap.value) isProgressMap.value = false;
    Get.dialog(
      AlertDialog(
        title: Text("Skip locations remaining!"),
        content: Text(
            "You have some location which is skip by you, do you want to navigate?"),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Get.back();
              _showCompleteJourneyDialog();
            },
            child: Text(
              "Completed",
            ),
          ),
          TextButton(
            onPressed: () {
              Get.back();
              var listOfSkip = listOfSkipSegmentLocation.reversed.toList();
              listOfSegmentLocation.clear();
              listOfSegmentLocation.addAll(listOfSkip);
              _setPolyLines();
            },
            child: Text(
              "Navigate",
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }
}
