import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animarker/widgets/animarker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:geolocator/geolocator.dart' as geolocator;
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as locationImport;
import 'package:maps_toolkit/maps_toolkit.dart' as mapTool;
import 'package:skeleton_flutter/gen/colors.gen.dart';
import 'package:skeleton_flutter/pages/home/home_controller.dart';
import 'package:skeleton_flutter/pages/home/list_travel_address.dart';
import 'package:skeleton_flutter/pages/home/location_remaining.dart';
import 'package:skeleton_flutter/pages/navigation/navigation_controller.dart';
import 'package:skeleton_flutter/pages/notification/notification.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'package:skeleton_flutter/util/touch_handle.dart';

class NavigationSource extends StatefulWidget {
  const NavigationSource({Key? key}) : super(key: key);

  @override
  _NavigationSourceState createState() => _NavigationSourceState();
}

class _NavigationSourceState extends State<NavigationSource> {
  final NavigationController _controller = Get.put(NavigationController());

  static const double CAMERA_ZOOM = 18;
  static const double CAMERA_TILD = 60;
  static const int LOCATION_INTERVAL = 3000;
  num CAMERA_BEARING = 0.0;
  late GoogleMapController mapController;
  late locationImport.PermissionStatus _permissionGranted;
  late StreamSubscription<locationImport.LocationData> locationSubscription;
  final controllerNewMap = Completer<GoogleMapController>();
  var _mapType = MapType.normal;
  var lastIndexReached = -1;

  dynamic _arguemntsFromPrev;

  // wrapper around the location API
  late locationImport.Location location;
  bool _serviceEnabled = false;
  bool isNavigationStarted = false;

  void _onMapCreated(GoogleMapController controller) {
    controllerNewMap.complete(controller);
    mapController = controller;
    location = new locationImport.Location();
    location.changeSettings(interval: LOCATION_INTERVAL, distanceFilter: 5);
    locationSubscription =
        location.onLocationChanged.listen((locationImport.LocationData cLoc) {
      //Your code
      print("Location Change $cLoc");
      _controller.currentLocation = cLoc;
      updateCurrentPinOnMap();
    });
    setInitialLocation();
  }

  void setInitialLocation() async {
    // set the initial location by pulling the user's
    // current location from the location's getLocation()
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == locationImport.PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != locationImport.PermissionStatus.granted) {
        return;
      }
    }
    _controller.currentLocation = await location.getLocation();
    _controller.getUserLatLngFromServer(_arguemntsFromPrev);
    updateCurrentPinOnMap();
  }

  void updateCurrentPinOnMap() async {
    /* LatLng destinationLat;
    if (_controller.allPolylineCoordinates.isNotEmpty)
      destinationLat = _controller.allPolylineCoordinates[0];
    else
      destinationLat = LatLng(_controller.currentLocation.latitude ?? 0.0,
          _controller.currentLocation.longitude ?? 0.0);

    mapTool.LatLng toLat =
        mapTool.LatLng(destinationLat.latitude, destinationLat.longitude);*/
    mapTool.LatLng fromLat = mapTool.LatLng(
        _controller.currentLocation.latitude ?? 0.0,
        _controller.currentLocation.longitude ?? 0.0);

    //var bearing = mapTool.SphericalUtil.computeHeading(fromLat, toLat);

    var indexRemove = mapTool.PolygonUtil.locationIndexOnPath(
        fromLat, _controller.allPolylineCoordinatesMapTool, true,
        tolerance: 7);

    ///for getting index mapTool.PolygonUtil.locationIndexOnPath(point, poly, geodesic)
    ///For Shadow Latlong  mapTool.SphericalUtil.computeOffset(from, distance, heading)
    if (indexRemove != -1) {
      lastIndexReached = indexRemove;
      mapTool.LatLng toLatestLat = mapTool.LatLng(
          _controller.allPolylineCoordinatesMapTool[indexRemove].latitude,
          _controller.allPolylineCoordinatesMapTool[indexRemove].longitude);
      CAMERA_BEARING =
          mapTool.SphericalUtil.computeHeading(fromLat, toLatestLat);
      //bearing = mapTool.SphericalUtil.computeHeading(toLatestLat, fromLat);
      CAMERA_BEARING = CAMERA_BEARING + 180;
    } else {
      ///Recalculate Path if user try to move diff location (Re-route)
      if (lastIndexReached != -1 && _controller.allPolylineCoordinatesMapTool.length > lastIndexReached) {
        var lastIndexPos =
            _controller.allPolylineCoordinatesMapTool[lastIndexReached];
        var distance =
            mapTool.SphericalUtil.computeDistanceBetween(fromLat, lastIndexPos);
        //_controller.errorToast("Distance $distance");
        if (distance > 200) {
          lastIndexReached = -1;
          _calculateAgainPath();
        }
      }else{
        //Put logic like user still getting this
      }
    }
    //var bearing = mapTool.SphericalUtil.computeHeading(fromLat, toLat);

    ///SHadow LatLng for bottom showing pin
    var shadowLatLng =
        mapTool.SphericalUtil.computeOffset(fromLat, 150, CAMERA_BEARING);

    CameraPosition cPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: CAMERA_BEARING as double,
        tilt: CAMERA_TILD,
        target: LatLng(shadowLatLng.latitude, shadowLatLng.longitude));
    if (!_controller.userTouchMap.value)
      mapController.animateCamera(CameraUpdate.newCameraPosition(cPosition));
    setState(() {
      // updated position
      var pinPosition = LatLng(_controller.currentLocation.latitude!,
          _controller.currentLocation.longitude!);
      _controller.currentMarkers.values
          .toList()
          .removeWhere((m) => m.markerId.value == NavigationController.SOURCE);
      _controller.addCurrentMarker(pinPosition);
    });
    print("Navigation Started $isNavigationStarted");
    if (isNavigationStarted) {
      /* var indexRemove = mapTool.PolygonUtil.locationIndexOnPath(
          fromLat, _controller.allPolylineCoordinatesMapTool, false,
          tolerance: 5);
      indexRemove = indexRemove - 1;
      //_controller.errorToast("Index Path Remove $indexRemove");
      if (indexRemove >= 0) {
        for (var i = indexRemove; i >= 0; i--) {
          _controller.allPolylineCoordinates.removeAt(i);
          _controller.allPolylineCoordinatesMapTool.removeAt(i);
        }
        Polyline polyline = Polyline(
          polylineId: PolylineId('poly'),
          color: Colors.blue,
          points: _controller.allPolylineCoordinates,
          width: 4,
        );

        // Adding the polyline to the map
        setState(() {
          _controller.polylines[PolylineId('poly')] = polyline;
        });
      }*/
      return;
      /* bool isContainPath = mapTool.PolygonUtil.containsLocation(
          mapTool.LatLng(_controller.currentLocation.latitude ?? 0.0,
              _controller.currentLocation.longitude ?? 0.0),
          listOfPoly,
          false);*/
      /* _controller.allPolylineCoordinates.removeWhere((element) =>
          calculateDistance(
              _controller.currentLocation.latitude!,
              _controller.currentLocation.longitude!,
              element.latitude,
              element.longitude) <
          15);*/

      if (_controller.calculateDistance(
              _controller.currentLocation.latitude!,
              _controller.currentLocation.longitude!,
              _controller.allPolylineCoordinates[0].latitude,
              _controller.allPolylineCoordinates[0].longitude) <
          15) {
        _controller.allPolylineCoordinates.removeAt(0);
        Polyline polyline = Polyline(
          polylineId: PolylineId('poly'),
          color: Colors.blue,
          points: _controller.allPolylineCoordinates,
          width: 4,
        );

        // Adding the polyline to the map
        setState(() {
          _controller.polylines[PolylineId('poly')] = polyline;
        });
      }
    }
  }

  void refreshMapWithPoly() async {
    //_openLocationBottomSheet();
    isNavigationStarted = true;
    var polyLine = Polyline(
        polylineId: PolylineId("poly"),
        color: Colors.blue,
        points: _controller.allPolylineCoordinates,
        width: 4);
    setState(() {
      _controller.polylines[PolylineId('poly')] = polyLine;
    });
  }

  void _getSpeedOfVehicle() {
    Geolocator.getPositionStream(
            forceAndroidLocationManager: true,
            intervalDuration: Duration(seconds: 3),
            distanceFilter: 2,
            desiredAccuracy: geolocator.LocationAccuracy.high)
        .listen((position) {
      var speedInMps = double.parse(position.speed.toStringAsPrecision(2)) +
          10; // this is your speed
      print("Speed -> $speedInMps");
      //_controller.errorToast("M -> ${_controller.maxSpeed} < S $speedInMps");
      if (double.parse(_controller.maxSpeed) < speedInMps) {
        if (speedInMps > 0) _controller.reportSpeedViolation(speedInMps);
      }
    });
  }

  _getMessage() {
    // workaround for onLaunch: When the app is completely closed (not in the background) and opened directly from the push notification
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      print("onMessage getInitialMessage data: ${message?.data}");
      // _controller.errorToast("getInitialMessage ${message?.data.toString()}");
    });

    // onMessage: When the app is open and it receives a push notification
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("onMessage data: ${message.data}");
      _controller.showNotificationDialog(message);
      // _controller.errorToast("onMessage ${message?.data.toString()}");
    });

    // replacement for onResume: When the app is in the background and opened directly from the push notification.
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('onMessageOpenedApp data: ${message.data}');
      // _controller.errorToast("onMessageOpenedApp ${message?.data.toString()}");
    });
  }

  @override
  void initState() {
    super.initState();
    _arguemntsFromPrev = Get.arguments;
    try {
      _controller.isMapRefresh.stream.listen((isRefresh) {
        if (isRefresh == MapRefreshType.REFRESH) {
          refreshMapWithPoly();
        } else if (isRefresh == MapRefreshType.RECENTER) {
          _recenterMap();
        }
      });
    } catch (e) {
      e.printError();
    }
    _getSpeedOfVehicle();
    _getMessage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "home".tr,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 24.sp),
          ),
          actions: _topMenu()),
      body: WillStartForegroundTask(
        onWillStart: () async {
          // You can add a foreground task start condition.
          return _controller.getGeofenceService().isRunningService;
        },
        androidNotificationOptions: const AndroidNotificationOptions(
          channelId: 'geofence_service_notification_channel',
          channelName: 'Geofence Service Notification',
          channelDescription:
              'This notification appears when the geofence service is running in the background.',
          channelImportance: NotificationChannelImportance.LOW,
          priority: NotificationPriority.LOW,
          isSticky: false,
        ),
        iosNotificationOptions: const IOSNotificationOptions(),
        notificationTitle: 'Geofence Service is running',
        notificationText: 'Tap to return to the app',
        child: Stack(
          children: [
            Animarker(
              mapId: controllerNewMap.future.then<int>((value) => value.mapId),
              useRotation: false,
              zoom: CAMERA_ZOOM,
              shouldAnimateCamera: false,
              //rippleRadius: 0.1,
              //[0,1.0] range, how big is the circle
              //rippleColor: ColorName.accentColor,
              // Color of fade ripple circle
              //rippleDuration: Duration(milliseconds: 2000),
              markers: Set.of(_controller.currentMarkers.values),
              child: GoogleMap(
                gestureRecognizers: Set()
                  ..add(Factory<OneSequenceGestureRecognizer>(
                      () => TouchHandle(() {
                            if (_controller.currentLocation.latitude != null)
                              _controller.userTouchMap.value = true;
                          }))),
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: LatLng(0, 0),
                  zoom: 11.0,
                ),
                markers: Set.of(_controller.markers.values),
                polylines: Set.of(_controller.polylines.values),
                mapType: _mapType,
                onTap: (latlng) {
                  _openLocationBottomSheet();
                },
              ),
            ),
            _topLocations(),
            _progressBar()
          ],
        ),
      ),
    );
  }

  void _openLocationBottomSheet() {
    if (_controller.listOfSegmentLocation.isEmpty) return;
    showModalBottomSheet(
        barrierColor: Colors.black.withAlpha(1),
        backgroundColor: Colors.white,
        context: context,
        builder: (context) {
          return LocationRemaining(
            listOfSegment: _controller.listOfSegmentLocation,
          );
        });
  }

  _topLocations() {
    return Positioned(
        top: 10.h,
        right: 10.h,
        left: 10.h,
        child: ListOfTravelAddress(
          listOfSegment: _controller.listOfSegmentLocation,
        ));
  }

  _progressBar() {
    return (_controller.isProgressShowing.value ||
            _controller.isProgressMap.value)
        ? Center(
            child: SizedBox(
                width: 30.r,
                height: 30.r,
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(ColorName.accentColor),
                  strokeWidth: 5.w,
                )),
          )
        : Container();
  }

  _topMenu() {
    return [
      IconButton(
        icon: const Icon(Icons.notifications, size: 32,),
        color: Colors.yellow[800],
        tooltip: 'Notifications',
        onPressed: () {
          Get.to(NotificationScreen());
          //_openPinDetailsBottomSheet();
        },
      ),
      SizedBox(width: 10,),
      IconButton(
        icon: const Icon(Icons.map, size: 32,),
        tooltip: 'Map Change',
        onPressed: () {
          setState(() {
            if (_mapType == MapType.normal) {
              _mapType = MapType.satellite;
            } else {
              _mapType = MapType.normal;
            }
          });
        },
      ),
      SizedBox(width: 10,),
      IconButton(
        icon: const Icon(Icons.my_location, size: 32,),
        tooltip: 'Recenter',
        onPressed: () {
          _recenterMap();
        },
      ),
      SizedBox(width: 10,),
      PopupMenuButton(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(Icons.more_vert, size: 32,),
        ),
        onSelected: (newValue) {
          // add this property
          if (newValue == 0) {
            Get.off(SegmentListScreen());
          } else
            _controller.logoutDialog();
        },
        itemBuilder: (context) => [
          PopupMenuItem(
            child: Text(
              "reroute".tr,
            ),
            value: 0,
          ),
          PopupMenuItem(
            child: Text(
              "logout".tr,
            ),
            value: 1,
          ),
        ],
      ),
    ];
  }

  @override
  void dispose() {
    super.dispose();
    locationSubscription.cancel();
    _controller.cancelAll();
  }

  void _recenterMap() {
    _controller.userTouchMap.value = false;
    CameraPosition cPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        tilt: CAMERA_TILD,
        target: LatLng(_controller.currentLocation.latitude!,
            _controller.currentLocation.longitude!));
    mapController.animateCamera(CameraUpdate.newCameraPosition(cPosition));
  }

  void _calculateAgainPath() {
    _controller.refreshPolyline();
  }
}
