
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/data/response/notification_list.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';

class NotificationController  extends BaseController{

  var listOfNotification = <Notifications>[].obs;

  @override
  void onInit() {
    super.onInit();
    _getNotificationList();
  }

  _getNotificationList() async{
    var segmentListData = await callApi(
        api.get(
            url: GET_NOTIFICATION + "/" + storage.getUserId()!,
            isCompleteUrl: true), onError: (e) {
    });
    if (segmentListData != null) {
      var apiResModel = ApiResModel.fromJson(segmentListData);
      var segmentData = NotificationData.fromJson(apiResModel.data);
      listOfNotification.clear();
      if(segmentData.notifications != null) {
        listOfNotification.addAll(segmentData.notifications!);
      }
    }
  }


}