import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';

import 'notification_controller.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final NotificationController _controller = Get.put(NotificationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            "notification".tr,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 24.sp),
          ),
        ),
        body: Obx(() => _controller.listOfNotification.length == 0
            ? _progressBar()
            : ListView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10),
                itemCount: _controller.listOfNotification.length,
                itemBuilder: (BuildContext context, int index) {
                  return _rowNotification(index);
                },
              )));
  }

  _progressBar() {
    return Obx(() => Center(
          child: (_controller.isProgressShowing.value)
              ? SizedBox(
                  width: 35.r,
                  height: 35.r,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(ColorName.accentColor),
                    strokeWidth: 5.w,
                  ))
              : Container(),
        ));
  }

  Widget _rowNotification(int index) {
    return Container(
        decoration: index == 0
            ? BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: ColorName.pinkColor)
            : null,
        margin: EdgeInsets.all(12.0),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${_controller.listOfNotification[index].title}\n${_controller.listOfNotification[index].message}",
                style: TextStyle(
                    fontSize: 14.sp, color: ColorName.greyTextfieldColor),
              ),
              SizedBox(
                height: 10.h,
              ),
              Text("${_controller.listOfNotification[index].receivedOn}",
                  style: TextStyle(
                      fontSize: 12.sp, color: ColorName.greyTextfieldColor))
            ],
          ),
        ));
  }
}
