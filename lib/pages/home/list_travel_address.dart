import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';

class ListOfTravelAddress extends StatelessWidget {
  final List<SegmentLocation> listOfSegment;

  const ListOfTravelAddress({
    Key? key,
    required this.listOfSegment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<SegmentLocation> lisOfSelected =
        listOfSegment.where((element) => element.completed == "0").toList();
    return lisOfSelected.length > 0
        ? Card(
            color: Colors.grey[200],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.all(0.0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: lisOfSelected.length > 3 ? 3 : lisOfSelected.length,
              itemBuilder: (context, pos) {
                return _topItemLocation(lisOfSelected[pos], pos);
              },
            ),
          )
        : Container();
  }

  _topItemLocation(SegmentLocation? segment, int pos) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(
                '${pos + 1}. ${segment?.address}',
                style: TextStyle(
                  fontSize: 16.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              )),
              SizedBox(
                width: 10,
              ),
              Text('${segment?.publications}',
                  style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w600,
                      color: Colors.black.withOpacity(0.8)))
            ],
          ),
          if (segment?.notes != null && segment?.notes?.isNotEmpty == true)
            Text('${segment?.notes}',
                style: TextStyle(
                    fontSize: 14.sp, color: Colors.black.withOpacity(0.8))),
        ],
      ),
    );
    return ListTile(
      dense: true,
      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
      visualDensity: VisualDensity(horizontal: 0, vertical: -4),
      title: Text(
        '${pos + 1}. ${segment?.address}',
        style: TextStyle(fontSize: 12.sp, color: Colors.black),
      ),
      subtitle: (segment?.notes != null && segment?.notes?.isNotEmpty == true)
          ? Text(
              'Publications: ${segment?.publications} \nNote: ${segment?.notes}',
              style: TextStyle(
                  fontSize: 10.sp, color: Colors.black.withOpacity(0.8)))
          : Text('Publications: ${segment?.publications}',
              style: TextStyle(
                  fontSize: 10.sp, color: Colors.black.withOpacity(0.8))),
    );
  }
}
