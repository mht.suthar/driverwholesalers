import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';

class LocationRemaining extends StatelessWidget {
  final List<SegmentLocation> listOfSegment;

  const LocationRemaining({
    Key? key,
    required this.listOfSegment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var completedCount =
        listOfSegment.where((element) => element.completed == "1").length;
    var uncompletedCount =
        listOfSegment.where((element) => element.completed == "0").length;
    var nextLocation =
        listOfSegment.firstWhere((element) => element.completed == "0");
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
              width: 80.0,
              child: Divider(
                color: ColorName.greyColor,
                thickness: 3,
              )),
          Row(
            children: [
              Expanded(
                  child: _roundCornerText("" + completedCount.toString(),
                      'location_comp'.tr, false)),
              Expanded(
                  child: _roundCornerText("" + uncompletedCount.toString(),
                      'location_remain'.tr, true))
            ],
          ),
          /*Text("Next Location: ${nextLocation.address}",
              style: TextStyle(
                  fontSize: 20.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.w400)),*/
        ],
      ),
    );
  }

  Widget _roundCornerText(String count, String msg, bool isRemaining) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color:
                isRemaining ? ColorName.accentColor : ColorName.splashBgColor),
        margin: EdgeInsets.all(8.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(count,
                  style: TextStyle(
                      fontSize: 14.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.w700)),
              SizedBox(width: 5),
              Expanded(
                child: Text(
                  msg,
                  style:
                      TextStyle(color: Colors.white, fontWeight: FontWeight.w400, fontSize: 12.sp,),
                ),
              )
            ],
          ),
        ));
  }
}
