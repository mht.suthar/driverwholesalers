import 'dart:async';
import 'dart:math';
import 'dart:math' as math;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animarker/widgets/animarker.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:geolocator/geolocator.dart' as geolocator;
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as locationImport;
import 'package:maps_toolkit/maps_toolkit.dart' as mapTool;
import 'package:skeleton_flutter/data/response/source_to_destination.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';
import 'package:skeleton_flutter/pages/home/location_remaining.dart';
import 'package:skeleton_flutter/pages/notification/notification.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'package:skeleton_flutter/util/touch_handle.dart';
import 'package:vector_math/vector_math.dart' as vectorMath;

import 'home_controller.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeController _controller = Get.put(HomeController());
  late GoogleMapController mapController;
  bool isStopped = false;
  var mapType = MapType.normal;

  double CAMERA_ZOOM = 18;
  double CAMERA_TILD = 60;
  double CAMERA_BEARING = 60;

  // wrapper around the location API
  late locationImport.Location location;

  bool _serviceEnabled = false;
  bool isNavigationStarted = false;
  late locationImport.PermissionStatus _permissionGranted;

  late StreamSubscription<locationImport.LocationData> locationSubscription;

  dynamic arguemntsFromPrev;

  final controllerNewMap = Completer<GoogleMapController>();

  void _onMapCreated(GoogleMapController controller) {
    controllerNewMap.complete(controller);
    mapController = controller;
    location = new locationImport.Location();
    location.changeSettings(interval: 3000, distanceFilter: 5);
    locationSubscription =
        location.onLocationChanged.listen((locationImport.LocationData cLoc) {
      //Your code
      print("Location Change $cLoc");
      _controller.currentLocation = cLoc;
      updateCurrentPinOnMap();
    });
    setInitialLocation();
  }

  @override
  void initState() {
    super.initState();
    arguemntsFromPrev = Get.arguments;
    try {
      _controller.isMapRefresh.stream.listen((isRefresh) {
        if (isRefresh == MapRefreshType.REFRESH) {
          _showInfoWindow();
          refreshMapWithPoly();
        } else if (isRefresh == MapRefreshType.INFO_WINDOW_SHOWN) {}
      });
    } catch (e) {
      e.printError();
    }
    _getSpeedOfVehicle();
    _getMessage();
  }

  _getMessage() {
    // workaround for onLaunch: When the app is completely closed (not in the background) and opened directly from the push notification
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      print("onMessage getInitialMessage data: ${message?.data}");
      // _controller.errorToast("getInitialMessage ${message?.data.toString()}");
    });

    // onMessage: When the app is open and it receives a push notification
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("onMessage data: ${message.data}");
      showNotificationDialog(message);
      // _controller.errorToast("onMessage ${message?.data.toString()}");
    });

    // replacement for onResume: When the app is in the background and opened directly from the push notification.
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('onMessageOpenedApp data: ${message.data}');
      // _controller.errorToast("onMessageOpenedApp ${message?.data.toString()}");
    });
  }

  @override
  void dispose() {
    super.dispose();
    locationSubscription.cancel();
    _controller.cancelAll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "home".tr,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 24.sp),
          ),
          actions: _topMenu()),
      body: WillStartForegroundTask(
        onWillStart: () async {
          // You can add a foreground task start condition.
          return _controller.getGeofenceService().isRunningService;
        },
        androidNotificationOptions: const AndroidNotificationOptions(
          channelId: 'geofence_service_notification_channel',
          channelName: 'Geofence Service Notification',
          channelDescription:
              'This notification appears when the geofence service is running in the background.',
          channelImportance: NotificationChannelImportance.LOW,
          priority: NotificationPriority.LOW,
          isSticky: false,
        ),
        iosNotificationOptions: const IOSNotificationOptions(),
        notificationTitle: 'Geofence Service is running',
        notificationText: 'Tap to return to the app',
        child: Stack(
          children: [
            Animarker(
              mapId: controllerNewMap.future.then<int>((value) => value.mapId),
              useRotation: false,
              zoom: CAMERA_ZOOM,
              shouldAnimateCamera: false,
              markers: Set.of(_controller.currentMarkers.values),
              child: GoogleMap(
                gestureRecognizers: Set()
                  ..add(Factory<OneSequenceGestureRecognizer>(
                      () => TouchHandle(() {
                            if (_controller.currentLocation.latitude != null)
                              _controller.userTouchMap.value = true;
                          }))),
                mapType: mapType,
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: LatLng(0, 0),
                  zoom: 11.0,
                ),
                markers: Set.of(_controller.markers.values),
                polylines: Set.of(_controller.polylines.values),
                onTap: (latlng) {
                  _openLocationBottomSheet();
                },
              ),
            ),
            _progressBar(),
            _topLocations()
          ],
        ),
      ),
    );
  }

  void _openLocationBottomSheet() {
    if (_controller.listOfSegmentLocation.isEmpty) return;
    showModalBottomSheet(
        barrierColor: Colors.black.withAlpha(1),
        backgroundColor: Colors.white,
        context: context,
        builder: (context) {
          return LocationRemaining(
            listOfSegment: _controller.listOfSegmentLocation,
          );
        });
  }

  _getBearing(LatLng begin, LatLng end) {
    double dLon = (end.longitude - begin.longitude);
    double x = math.sin(vectorMath.radians(dLon)) *
        math.cos(vectorMath.radians(end.latitude));
    double y = math.cos(vectorMath.radians(begin.latitude)) *
            math.sin(vectorMath.radians(end.latitude)) -
        math.sin(vectorMath.radians(begin.latitude)) *
            math.cos(vectorMath.radians(end.latitude)) *
            math.cos(vectorMath.radians(dLon));
    double bearing = vectorMath.degrees((math.atan2(x, y)));
    return bearing;
  }

  void updateCurrentPinOnMap() async {
    /**
     * Need to implement later to redraw path
     */
    LatLng destinationLat;
    if (_controller.allPolylineCoordinates.isNotEmpty)
      destinationLat = _controller.allPolylineCoordinates[0];
    else
      destinationLat = LatLng(_controller.currentLocation.latitude ?? 0.0,
          _controller.currentLocation.longitude ?? 0.0);

    mapTool.LatLng fromLat =
      mapTool.LatLng(destinationLat.latitude, destinationLat.longitude);
    mapTool.LatLng toLat = mapTool.LatLng(
        _controller.currentLocation.latitude ?? 0.0,
        _controller.currentLocation.longitude ?? 0.0);
    /*var distance = mapTool.SphericalUtil.computeDistanceBetween(fromLat, toLat);
    if (distance < 10) {
      return;
    }*/
    // compute our own bearing (do not use location bearing)
    var bearing = mapTool.SphericalUtil.computeHeading(toLat, fromLat);

    /*double center = _controller.currentLocation.latitude!;
    double southMap = await mapController
        .getVisibleRegion()
        .then((value) => value.southwest.latitude);
    double diff = (center - southMap);
    double newLat =
        _controller.currentMarkers[_controller.SOURCE]?.position.latitude ??
            0 + diff;

    LatLng? markerLat =
        _controller.currentMarkers[_controller.SOURCE]?.position;*/

    CameraPosition cPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        bearing: bearing as double,
        tilt: CAMERA_TILD,
        //bearing: _controller.currentLocation.heading ?? 0,
        target: LatLng(_controller.currentLocation.latitude!,
            _controller.currentLocation.longitude!)
        /*target: LatLng(markerLat?.latitude ?? 0 + 0.009,
            markerLat?.longitude ?? 0)*/
        /*target: LatLng(newLat,
            _controller.currentMarkers[_controller.SOURCE]?.position.longitude ?? 0.0)*/
        );

    if (!_controller.userTouchMap.value)
      mapController.animateCamera(CameraUpdate.newCameraPosition(cPosition));
    // do this inside the setState() so Flutter gets notified
    // that a widget update is due
    setState(() {
      // updated position
      var pinPosition = LatLng(_controller.currentLocation.latitude!,
          _controller.currentLocation.longitude!);
      _controller.currentMarkers.values
          .toList()
          .removeWhere((m) => m.markerId.value == _controller.SOURCE);
      _controller.addCurrentMarker(pinPosition);
    });

    if (isNavigationStarted) {
      /**
       * Main Logic to remove polyline when user walk
       */
      if (calculateDistance(
          _controller.currentLocation.latitude!,
          _controller.currentLocation.longitude!,
          _controller.allPolylineCoordinates[0].latitude,
          _controller.allPolylineCoordinates[0].longitude) <
          10) {
        _controller.allPolylineCoordinates.removeAt(0);
        Polyline polyline = Polyline(
          polylineId: PolylineId('poly'),
          color: Colors.blue,
          points: _controller.allPolylineCoordinates,
          width: 4,
        );

        // Adding the polyline to the map
        setState(() {
          _controller.polylines[PolylineId('poly')] = polyline;
        });
      }

      /*_controller.allPolylineCoordinates.removeWhere((element) =>
          calculateDistance(
              _controller.currentLocation.latitude!,
              _controller.currentLocation.longitude!,
              element.latitude,
              element.longitude) <
          15);

      Polyline polyline = Polyline(
        polylineId: PolylineId('poly'),
        color: Colors.blue,
        points: _controller.allPolylineCoordinates,
        width: 4,
      );

      // Adding the polyline to the map
      setState(() {
        _controller.polylines[PolylineId('poly')] = polyline;
      });*/
    }
  }

  void setInitialLocation() async {
    // set the initial location by pulling the user's
    // current location from the location's getLocation()
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == locationImport.PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != locationImport.PermissionStatus.granted) {
        return;
      }
    }
    _controller.currentLocation = await location.getLocation();
    //_controller.initGeofence();
    _controller.getUserLatLngFromServer(arguemntsFromPrev);
    updateCurrentPinOnMap();
  }

  /// Result is in Meter
  /// if you want meter just multiply by 1000. return 1000 * 12742 * asin(sqrt(a))
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  refreshMapWithPoly() {
    _openLocationBottomSheet();
    isNavigationStarted = true;
    var polyLine = Polyline(
        polylineId: PolylineId("poly"),
        color: Colors.blue,
        points: _controller.allPolylineCoordinates,
        width: 6);
    setState(() {
      _controller.polylines[PolylineId('poly')] = polyLine;
    });
  }

  _topLocations() {
    return Positioned(
        top: 10.h,
        right: 10.h,
        left: 10.h,
        child:  _topCard()
    );
  }

  _topCard(){
    List<SegmentLocation> lisOfSelected = _controller.listOfSegmentLocation
        .where((element) => element.completed == "0").toList();
    return lisOfSelected.length > 0 ? Card(
      color: ColorName.accentColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      margin: EdgeInsets.all(0.0),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount:
        lisOfSelected.length > 3 ? 3 : lisOfSelected.length,
        itemBuilder: (context, pos) {
          return _topItemLocation(lisOfSelected[pos], pos);
        },
      ),
    ) : Container();
  }

  _topItemLocation(SegmentLocation? segment, int pos) {
    return  ListTile(
      dense:true,
      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
      visualDensity: VisualDensity(horizontal: 0, vertical: -4),
      title: Text(
        '${pos + 1}. ${segment?.address}',
        style: TextStyle(fontSize: 12.sp, color: Colors.white),
      ),
      //subtitle: Text("-"),
    );
  }

  _progressBar() {
    return Obx(() => Positioned(
          top: 16.h,
          left: 0,
          right: 0,
          child: Center(
            child: (_controller.isProgressShowing.value ||
                    _controller.isProgressMap.value)
                ? SizedBox(
                    width: 35.r,
                    height: 35.r,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(ColorName.accentColor),
                      strokeWidth: 5.w,
                    ))
                : Container(),
          ),
        ));
  }

  _topMenu() {
    return [
      IconButton(
        icon: const Icon(Icons.notifications),
        color: Colors.yellow[800],
        tooltip: 'Notifications',
        onPressed: () {
          Get.to(NotificationScreen());
          //_openPinDetailsBottomSheet();
        },
      ),
      IconButton(
        icon: const Icon(Icons.map),
        tooltip: 'Map Change',
        onPressed: () {
          setState(() {
            if(mapType == MapType.normal){
              mapType = MapType.satellite;
            }else{
              mapType = MapType.normal;
            }
          });
        },
      ),
      IconButton(
        icon: const Icon(Icons.my_location),
        tooltip: 'Recenter',
        onPressed: () {
          _controller.userTouchMap.value = false;
          CameraPosition cPosition = CameraPosition(
              zoom: CAMERA_ZOOM,
              tilt: CAMERA_TILD,
              target: LatLng(_controller.currentLocation.latitude!,
                  _controller.currentLocation.longitude!));
          mapController
              .animateCamera(CameraUpdate.newCameraPosition(cPosition));
        },
      ),
      PopupMenuButton(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(Icons.more_vert),
        ),
        onSelected: (newValue) {
          // add this property
          if (newValue == 0) {
            Get.off(SegmentListScreen());
          } else
            _controller.logoutDialog();
        },
        itemBuilder: (context) => [
          PopupMenuItem(
            child: Text(
              "reroute".tr,
            ),
            value: 0,
          ),
          PopupMenuItem(
            child: Text(
              "logout".tr,
            ),
            value: 1,
          ),
        ],
      ),
    ];
  }

  void _getSpeedOfVehicle() {
    Geolocator.getPositionStream(
            forceAndroidLocationManager: true,
            intervalDuration: Duration(seconds: 3),
            distanceFilter: 2,
            desiredAccuracy: geolocator.LocationAccuracy.high)
        .listen((position) {
      var speedInMps =
          position.speed.toStringAsPrecision(2); // this is your speed
      print("Speed -> $speedInMps");
      if (double.parse(_controller.maxSpeed) < double.parse(speedInMps)) {
        _controller.reportSpeedViolation(speedInMps);
      }
    });
  }

  _setTimer() {
    Timer.periodic(Duration(seconds: 5), (timer) {
      if (isStopped) {
        timer.cancel();
      }
    });
  }

  _showInfoWindow() async {
    /*_controller.markers.forEach((key, value) async{
       print("Marker Key for showMarkerInfoWindow $key");
       await mapController.showMarkerInfoWindow(MarkerId(key));
     });
     setState(() {});*/
  }

  void showNotificationDialog(RemoteMessage message) {
    Get.dialog(
      AlertDialog(
        title: Text("notification".tr),
        content: Text("${message.data}"),
        actions: <Widget>[
          TextButton(
            onPressed: () {

            },
            child: Text(
              "ok".tr,
            ),
          ),
        ],
      ),
      barrierDismissible: true,
    );
  }
}
