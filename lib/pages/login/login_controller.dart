import 'package:dio/dio.dart' as dio;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:skeleton_flutter/constants/api/api.dart';
import 'package:skeleton_flutter/data/response/api_common_res.dart';
import 'package:skeleton_flutter/pages/base/base_controller.dart';
import 'package:skeleton_flutter/pages/segment/segment_list.dart';
import 'package:skeleton_flutter/util/extensions.dart';

class LoginController  extends BaseController{

  final TextEditingController _tcEmailId = TextEditingController();
  final TextEditingController _tcPassword = TextEditingController();

  TextEditingController get tcEmailId => _tcEmailId;
  TextEditingController get tcPassword => _tcPassword;

  bool isValid() {
    if (_tcEmailId.text.trim().isEmpty) {
      errorToast('e_enter_email'.tr);
      return false;
    }

    if (!_tcEmailId.text.trim().isEmailValid) {
      errorToast('e_invalid_email'.tr);
      return false;
    }

    if (_tcPassword.text.isEmpty) {
      errorToast('e_enter_password'.tr);
      return false;
    }
    return true;
  }

  loginCall() async{
    if(isValid()){
      isProgressShowing.value = true;
      var token = await FirebaseMessaging.instance.getToken();
      print("Firebase Token $token");

      var formData = dio.FormData.fromMap({
        "username": _tcEmailId.text.trim(),
        "password": _tcPassword.text.trim(),
        "device_id": token,
      });
      var loginData = await callApi(api.post(url: LOGIN_API, data: formData)); //remove onError if error should be handled automatically (i.e. toast)
      if (loginData != null) {
        var apiResModel = ApiResModel.fromJson(loginData);
        if(apiResModel.status == true) {
          var driverId = (apiResModel.data as Map)["driverid"];
          storage.setUserID(driverId);
          Get.off(SegmentListScreen());
        }else{
          errorToast('your_email_pass_are_wrong'.tr);
        }
      }
    }
  }

  bool isLogIn() {
    return storage.getUserId() != null;
  }

  void clearText() {
    _tcEmailId.text = "";
    _tcPassword.text = "";
  }
}