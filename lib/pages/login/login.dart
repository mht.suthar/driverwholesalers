import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:skeleton_flutter/gen/assets.gen.dart';
import 'package:skeleton_flutter/gen/colors.gen.dart';

import 'login_controller.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final LoginController _controller = Get.put(LoginController());

  @override
  void initState() {
    super.initState();
    if (!kReleaseMode) {
      //_controller.tcEmailId.text = "mm@mm.com";
      //_controller.tcPassword.text = "123456";
    }
  }

  @override
  void dispose() {
    super.dispose();
    _controller.clearText();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Assets.res.drawables.bgBlueLogin.assetName),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                height: 120.h,
              ),
              Text(
                "Sign In",
                style: TextStyle(
                    fontSize: 55.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 60.h,
              ),
              Padding(
                padding: EdgeInsets.all(50.w),
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 35.h),
                      child: Container(
                        width: double.infinity,
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                            image: AssetImage(
                                Assets.res.drawables.bgLogin.assetName),
                            fit: BoxFit.fill,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 40.h,
                              ),
                              Text(
                                "Welcome Back!",
                                style: TextStyle(
                                    fontSize: 20.sp,
                                    color: ColorName.accentColor,
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: 30.h,
                              ),
                              TextField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          borderSide: BorderSide.none),
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 10.0, horizontal: 10.0),
                                      filled: true,
                                      hintStyle: TextStyle(
                                          color: ColorName.greyTextfieldColor),
                                      hintText: "Enter Email Id",
                                      fillColor: ColorName.greyColor,
                                      labelStyle: TextStyle(
                                        fontSize: 16.sp,
                                      )),
                                  keyboardType: TextInputType.emailAddress,
                                  controller: _controller.tcEmailId,
                                  textInputAction: TextInputAction.next),
                              SizedBox(
                                height: 10.0,
                              ),
                              TextField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        borderSide: BorderSide.none),
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    filled: true,
                                    hintStyle: TextStyle(
                                        color: ColorName.greyTextfieldColor),
                                    hintText: "Enter Password",
                                    fillColor: ColorName.greyColor,
                                    labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                    )),
                                obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                                textInputAction: TextInputAction.done,
                                controller: _controller.tcPassword,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Text(
                                "Forget Password?",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: ColorName.greyTextfieldColor),
                              ),
                              SizedBox(
                                height: 50.h,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Center(
                        child: Container(
                          height: 85.0,
                          width: 85.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(
                                  width: 5, color: ColorName.splashBgColor),
                              color: ColorName.accentColor),
                          child: Obx(() => _controller.isProgressShowing.value
                              ? SizedBox(
                                  width: 16.r,
                                  height: 16.r,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                    strokeWidth: 2.w,
                                  ))
                              : IconButton(
                                  icon: RotatedBox(
                                      quarterTurns: 90,
                                      child: const Icon(
                                        Icons.arrow_back_sharp,
                                        size: 40.0,
                                      )),
                                  color: Colors.white,
                                  tooltip: 'Next',
                                  onPressed: () {
                                    _controller.loginCall();
                                    //_openPinDetailsBottomSheet();
                                  },
                                )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
