class ApiResModel {
	dynamic data;
	String? image;
	bool? status;

	ApiResModel({this.data, this.image, this.status});

	ApiResModel.fromJson(Map<String, dynamic> json) {
		data = json['data'] != null ? json['data'] : null;
		image = json['image'] != null ? json['image'] : null;
		status = json['status'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.data != null) {
			data['data'] = this.data;
		}
		if (this.image != null) {
			data['image'] = this.image;
		}
		data['status'] = this.status;
		return data;
	}
}