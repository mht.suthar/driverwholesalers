# skeleton_flutter

A new Flutter application.

Old
keytool -genkey -v -keystore D:\bsmbooks(com.bsm.books).jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias bsmbooks

New
keytool -importkeystore -srckeystore D:\bsmbooks(com.bsm.books).jks -destkeystore D:\bsmbooks(com.bsm.books).jks -deststoretype pkcs12

pass : com.basm.books
alias : basmbooks
name and all : basbooks

# Build Relase APK
flutter build apk --release

# Build App Bundle
flutter build appbundle

# For Launcher icon generate
flutter pub run flutter_launcher_icons:main

# Command for Generate Asset color and more
fluttergen -c /pubspec.yaml

# For package name change
project.pbxproj -> PRODUCT_BUNDLE_IDENTIFIER -> change name
build.gradle -> change package name